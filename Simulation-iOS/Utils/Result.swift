//
//  Result.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 26/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

enum Result<Value> {
    case success(Value)
    case failure(Error)
}
