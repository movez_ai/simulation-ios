//
//  Data+Parsing.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 07/07/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

extension Data {
    
    func parseJSON<T: Codable>(toType type: T.Type) -> Result<T> {
        let decoder = JSONDecoder()
        do {
            let decodedObject = try decoder.decode(T.self, from: self)
            return .success(decodedObject)
        } catch {
            return .failure(error)
        }
    }
    
}
