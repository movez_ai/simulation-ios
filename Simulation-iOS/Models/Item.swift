//
//  Item.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

class Item: Codable {
    let guid: String
    let title: String
    let description: String
    
    init(guid: String,
         title: String,
         description: String) {
        self.guid = guid
        self.title = title
        self.description = description
    }
}






