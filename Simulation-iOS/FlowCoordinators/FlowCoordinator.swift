//
//  FlowCoordinator.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

protocol FlowCoordinator: class {
    var identifier: UUID { get }
    associatedtype FinishedResult
    typealias FinishedClosure = (FinishedResult) -> ()
    func setup(_ finishedClosure: @escaping FinishedClosure)
    func start()
}
