//
//  GeneralInfoFlowCoordinator.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class GeneralInfoFlowCoordinator: BaseFlowCoordinator<Void> {
    
    // MARK: - Properties
    
    private let navController: UINavigationController
    typealias Dependencies = HasApplicationData
    private let dependencies: Dependencies
       
    // MARK: - Lifecycle
    
    init(navController: UINavigationController, dependencies: Dependencies) {
        self.navController = navController
        self.dependencies = dependencies
        navController.isNavigationBarHidden = true
    }
    
    // MARK: - FlowCoordinator
    
    override func start() {
        let generalInfoVC = GeneralInfoViewController.instantiate(applicationData: dependencies.applicationData)
        navController.viewControllers = [generalInfoVC]
    }        
}
