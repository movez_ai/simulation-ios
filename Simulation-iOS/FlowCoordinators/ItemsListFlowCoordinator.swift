//
//  ItemsListFlowCoordinator.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class ItemsListFlowCoordinator: BaseFlowCoordinator<Void> {
    
    // MARK: - Properties
    
    private var navController: UINavigationController
    typealias Dependencies = HasApplicationData & HasNewsService
    private let dependencies: Dependencies
    
    // MARK: - Lifecycle
    
    init(navController: UINavigationController, dependencies: Dependencies) {
        self.navController = navController
        self.dependencies = dependencies
    }
    
    // MARK: - FlowCoordinator
    
    override func start() {
        let itemListVC = ItemsListViewController.instantiate(flowDelegate: self, dependencies: dependencies)
        navController.viewControllers = [itemListVC]
    }
    
    // MARK: - Private
    
    func showDetailedItem(for item: Item) {
        let detailedItemVC = DetailedItemViewController.instantiate(item: item)
        navController.show(detailedItemVC, sender: self)
    }
}

extension ItemsListFlowCoordinator: ItemsListViewControllerFlowDelegate {
    
    func itemsListViewControllerItemSelected(item: Item) {
        showDetailedItem(for: item)
    }
}
