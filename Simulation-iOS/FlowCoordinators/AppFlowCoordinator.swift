//
//  AppFlowCoordinator.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class AppFlowCoordinator: BaseFlowCoordinator<Void> {
    
    // MARK: - Properties
    
    private let window: UIWindow
    private var rootController: UIViewController
    private let dependencies: AppDependencies
    
    // MARK: - Helpers
    
    private enum Tab: CaseIterable {
        case generalInfo
        case itemsList        
        
        var title: String {
            switch self {
            case .generalInfo: return "General Info"
            case .itemsList: return "Items List"
            }
        }
    }
    
    // MARK: - Lifecycle
    
    init(window: UIWindow) {
        self.window = window
        rootController = UINavigationController()
        dependencies = AppDependencies(newsService: NewsService(), applicationData: ApplicationData())
    }
    
    // MARK: - FlowCoordinator
    
    override func start() {
        startMainFlow()
    }
    
    // MARK: - Private
    
    private func startMainFlow() {
        let tabBarController = UITabBarController()
        let tabs = Tab.allCases
        
        let navControllers = tabs
            .map { tab -> UINavigationController in
                let navController = UINavigationController()
                navController.title = tab.title
                navController.tabBarItem = UITabBarItem(title: tab.title, image: nil, selectedImage: nil)
                return navController
        }
        
        tabBarController.viewControllers = navControllers
        tabBarController.view.backgroundColor = UIColor.white
        
        updateRootController(with: tabBarController)
        
        zip(tabs, navControllers).forEach { (tab, navController) in
            switch tab {
            case .generalInfo:
                showGeneralInfo(with: navController)
            case .itemsList:
                showItemsList(with: navController)
            }
        }
    }
    
    private func showGeneralInfo(with navigationController: UINavigationController) {
        coordinate(to: GeneralInfoFlowCoordinator(navController: navigationController, dependencies: dependencies))
    }
    
    private func showItemsList(with navigationController: UINavigationController) {
        coordinate(to: ItemsListFlowCoordinator(navController: navigationController, dependencies: dependencies))
    }
    
    private func updateRootController(with viewController: UIViewController) {
        rootController = viewController
        window.rootViewController = rootController
    }
}
