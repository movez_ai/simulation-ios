//
//  BaseFlowCoordinator.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

class BaseFlowCoordinator<FinishedResultType>: FlowCoordinator {
    
    typealias FinishedResult = FinishedResultType
    let identifier = UUID()
    var finishedClosure: ((FinishedResultType) -> ())?
    private var childCoordinators = [UUID : Any]()
    
    // MARK: - Store and Release
    
    private func store<T: FlowCoordinator>(coordinator: T) {
        childCoordinators[coordinator.identifier] = coordinator
    }
    
    private func release<T: FlowCoordinator>(coordinator: T) {
        childCoordinators[coordinator.identifier] = nil
    }
    
    // MARK: - Coordinating
    
    func coordinate<T: FlowCoordinator>(to coordinator: T, finishedClosure: T.FinishedClosure? = nil) {
        store(coordinator: coordinator)
        coordinator.setup { [weak self, weak coordinator] (result) in
            if let coordinator = coordinator {
                self?.release(coordinator: coordinator)
            }
            finishedClosure?(result)
        }
        coordinator.start()
    }
    
    func setup(_ finishedClosure: @escaping FinishedClosure) {
        self.finishedClosure = finishedClosure
    }
    
    func start() {
        fatalError("start() method should be implemented in subclass.")
    }
}
