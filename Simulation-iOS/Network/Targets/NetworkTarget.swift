//
//  NetworkTarget.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 26/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

protocol NetworkTarget {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var params: Data? { get }
    var headers: [String: String]? { get }
}
