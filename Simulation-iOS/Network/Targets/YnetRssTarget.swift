//
//  YnetTarget.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 26/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

enum YnetRssTarget: NetworkTarget {
    case news
    
    // MARK: - NetworkTarget
    
    var baseURL: URL {
        return URL(string: "http://localhost:3000")!
    }
    
    var path: String {
        var path = "/api/ynet/"
        switch self {
        case .news:
            path.append(contentsOf: "news")
        }
        return path
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var params: Data? {
        return nil
    }
    
    var headers: [String : String]? {
        return nil
    }
}
