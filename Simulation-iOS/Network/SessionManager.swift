//
//  SessionManager.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 26/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

class SessionManager {
    
    let session = URLSession(configuration: URLSessionConfiguration.default)
    
    // MARK: - Life cycle
    
    deinit {
        session.invalidateAndCancel()
    }
    
    // MARK: - Public
    
    func performRequest(from target: NetworkTarget, completion: @escaping (Result<Data>) -> ()) {
        
        let urlRequest = URLRequest(target: target)
        
        session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            if let data = data {
                completion(.success(data))
            }
            else if let error = error {
                completion(.failure(error))
            }
        }.resume()
    }
    
}

extension URLRequest {
    
    init(target: NetworkTarget) {
        self = URLRequest(url: URL(target: target))
        self.httpMethod = target.method.rawValue
        self.allHTTPHeaderFields = target.headers
        self.httpBody = target.params
    }
}

extension URL {
    
    init(target: NetworkTarget) {
        if target.path.isEmpty {
            self = target.baseURL
        } else {
            self = target.baseURL.appendingPathComponent(target.path)
        }
    }
}
