//
//  AppDependencies.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 27/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

protocol HasNewsService {
    var newsService: NewsServiceType { get }
}

protocol HasApplicationData {
    var applicationData: ApplicationData { get }
}

struct AppDependencies: HasNewsService, HasApplicationData {
    let newsService: NewsServiceType
    let applicationData: ApplicationData
}
