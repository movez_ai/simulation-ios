//
//  Storyboard.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

enum Storyboard: String {
    case Main    
    
    func instantiate<VC: UIViewController>(viewController: VC.Type) -> VC {
        guard let vc = UIStoryboard(name: rawValue, bundle: Bundle(for: viewController)).instantiateViewController(withIdentifier: String.init(describing: viewController)) as? VC else {
            fatalError("Failed instantiating view controller: \(viewController) from storyboard: \"\(rawValue)\"")
        }
        return vc
    }
}
