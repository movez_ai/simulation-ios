//
//  DetailedItemViewController.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class DetailedItemViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    private var item: Item!
    
    // MARK: - Init
    
    static func instantiate(item: Item) -> DetailedItemViewController {
        let detailedItemViewController = Storyboard.Main.instantiate(viewController: DetailedItemViewController.self)
        detailedItemViewController.item = item
        return detailedItemViewController
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        populateLabels()
    }
    
    // MARK: - Private
    
    private func populateLabels() {
        titleLabel.text = item.title
        descriptionLabel.text = item.description
    }

}
