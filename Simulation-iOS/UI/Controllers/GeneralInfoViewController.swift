//
//  GeneralInfoViewController.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

class GeneralInfoViewController: UIViewController {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var selectedItemLabel: UILabel!
    
    private let dateFormatter: DateFormatter
    private var applicationData: ApplicationData?
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "iw")
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .medium
        super.init(coder: aDecoder)
    }
    
    static func instantiate(applicationData: ApplicationData) -> GeneralInfoViewController {
        let vc = Storyboard.Main.instantiate(viewController: GeneralInfoViewController.self)
        vc.applicationData = applicationData
        return vc
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = "<YOUR NAME>"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
        dateLabel.text = dateFormatter.string(from: Date())
        selectedItemLabel.text = applicationData?.selectedItem?.title
    }
}
