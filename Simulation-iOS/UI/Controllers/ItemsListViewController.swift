//
//  ItemsListViewController.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 24/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import UIKit

protocol ItemsListViewControllerFlowDelegate: class {
    func itemsListViewControllerItemSelected(item: Item)
}

class ItemsListViewController: UIViewController {
    
    @IBOutlet weak var itemsTableView: UITableView!
    
    weak var flowDelegate: ItemsListViewControllerFlowDelegate?
    private var items = [Item]()
    
    typealias Dependencies = HasApplicationData & HasNewsService
    private var dependencies: Dependencies!
    
    
    // MARK: - Init
    
    static func instantiate(flowDelegate: ItemsListViewControllerFlowDelegate, dependencies: Dependencies) -> ItemsListViewController {
        let vc = Storyboard.Main.instantiate(viewController: ItemsListViewController.self)
        vc.flowDelegate = flowDelegate
        vc.dependencies = dependencies
        return vc
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = navigationController?.title
        itemsTableView.tableFooterView = UIView()        
        
        dependencies.newsService.fetchNews { [weak self] (result) in
            switch result {
            case .success(let items):
                self?.items = items
                OperationQueue.main.addOperation {
                    self?.itemsTableView.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }

}

extension ItemsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if let dequeuedCell = tableView.dequeueReusableCell(withIdentifier: "Cell") {
            cell = dequeuedCell
        }
        else {
            cell = UITableViewCell.init(style: .value1, reuseIdentifier: "Cell")
        }
        
        let item = items[indexPath.row]
        cell.textLabel?.text = item.title
        
        return cell
    }
    
}

extension ItemsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = items[indexPath.row]
        dependencies.applicationData.selectedItem = item
        flowDelegate?.itemsListViewControllerItemSelected(item: item)
    }
}
