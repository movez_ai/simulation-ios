//
//  NewsService.swift
//  Simulation-iOS
//
//  Created by Ofer Meroz on 26/04/2019.
//  Copyright © 2019 Zemingo. All rights reserved.
//

import Foundation

protocol NewsServiceType {
    func fetchNews(completion: @escaping  (Result<[Item]>) -> ())
}


class NewsService: NewsServiceType {
    
    let sessionManager = SessionManager()
    
    func fetchNews(completion: @escaping (Result<[Item]>) -> ()) {
        
        sessionManager.performRequest(from: YnetRssTarget.news) { (result) in
            switch result {
            case .success(let data):                
                completion(data.parseJSON(toType: [Item].self))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
